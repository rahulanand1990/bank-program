package BankProgram;

public interface BankInterface 
{
    public String getName(); // returns name of account holder
    
    public void setName(String name); // sets name of account holder
    
    public int getAccountNo(); // returns account number
    
    public void setAccountNo(int ac); // sets account number
    
    public String getAddress(); // returns address of account holder
    
    public void setAddress(String addr); // sets new address
    
    public double getBalance(); // returns balance of account holder
    
    public void setBalance(double b); // sets balance of account holder
    
    public double creditBalance(double amount); // credits balance by given amouont
    
    public boolean debitBalance(double amount); // debits balance by given amount
}