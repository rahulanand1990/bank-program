package BankProgram;

import java.util.StringTokenizer;

public class PersonalInformation {

	private String name;
	private String address;
	private String father;
	private String mother;
	private String child;
	private String spouse;
		
	
	/**
	 * Constructor to initialise the parameters
	 * @param i counter
	 * @param a Address
	 * @param f Father's name
	 * @param m Mother's name
	 * @param c Child's name
	 * @param s Spouse's name
	 */
	
	public PersonalInformation(String n,String a, String f, String m, String c, String s)
	{
		this.name=n;
		this.address=a;
		this.father=f;		
		this.mother=m;		
		this.spouse=s;
		this.child=c;
	}
	
	/**
	 * Returns address of the account holder 
	 * @return Address
	 */
	
	public String getAddress()
	{
		return this.address;
	}
	
	/**
	 * Returns child name of the account holder 
	 * @return Child Name
	 */
	
	public String getChild()
	{
		return this.child;
	}
	
	/**
	 * Returns spouse's name of the account holder 
	 * @return Spouse Name
	 */
	
	public String getSpouse()
	{
		return this.spouse;
	}
	
	/**
	 * Returns father's name of the account holder 
	 * @return Father Name
	 */
	
	public String getFather()
	{
		return this.father;
	}
	
	/**
	 * Returns mother's name of the account holder 
	 * @return Mother's Name
	 */
	
	public String getMother()
	{
		return this.mother;
	}
	
	/**
	 * Returns name of the account holder 
	 * @return Name
	 */
	
	
	public String getName()
	{
		return this.name;
	}
	
	/**
	 * Sets the name of the account holder
	 * @param n Name of the account holder
	 */
	
	public void setName(String n)
	{
		this.name=n;
	}
	
	public void setAddress(String n)
	{
		this.address=n;
	}
	
	
	/**
	 * Overrides the default toString method to an output of user's choice
	 */
	
	public String toString()
	{
		String s= "Name = " + this.getName()+ "\nAddress = "+ this.getAddress()+ "\nFather's name = "+ this.getFather(); 
		String s1="\nMother's name = " + this.getMother() + "\nSpouse name = "+ this.getSpouse()+ "\nChild's name = "+ this.getChild();
		
		return (s+s1);
	}
	
	/**
	 * Splits the name into 3 parts using "space" as the delimiter
	 */
	
	public void nameToken()
	{
		String firstName="",midName="",lastName=""; 

		StringTokenizer st = new StringTokenizer(this.getName());
			
		firstName=st.nextToken();
		if(st.hasMoreTokens())
				midName=st.nextToken();
	    	 
		while (st.hasMoreTokens()) 
			lastName=lastName+st.nextToken();
			
		if(firstName!="")
			System.out.println("First name = "  + firstName);
		
		if(midName!="")
			System.out.println("Middle name = "  + midName);
		
		if(lastName!="")
			System.out.println("Last name = "  + lastName);
		
//		System.out.println("\n");
		
		//System.out.println("First name = " + firstName + " Middle name = " + midName + " Last name = " + lastName + "\n");
	}


}
