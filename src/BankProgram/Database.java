package BankProgram;

import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.TreeMap;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;


public class Database extends JFrame {

	private static JTextField nameField, accField, addField, balField, amtField; //Input fields
	
	private static JLabel nameLabel, accLabel, addLabel, balLabel; //Text labels on frame
	
	private static JButton nextButton, lastButton, newButton, saveButton, exitButton, findButton, withdrawButton, depositButton;
	
	private JPanel topPanel, middlePanel, bottomPanel, rootPanel, newPanel, labPanel;
	
	int flag=0;
	int counter=0;
	
	public Database() throws ClassNotFoundException, SQLException
	{
		super("BANK"); //to give caption BANK to GUI
		
				
		/*Initialising text fields*/
		nameField = new JTextField();
		accField = new JTextField(); 
		addField = new JTextField();
		balField = new JTextField();
		amtField = new JTextField();
		
		/*Initialising text labels*/
		nameLabel = new JLabel("Name");
		accLabel = new JLabel("Account No");
		addLabel = new JLabel("Address");
		balLabel = new JLabel("Balance");
		
		/*declaring and giving captions to buttons*/
		nextButton = new JButton(">>");
		lastButton = new JButton(">>>");
		newButton = new JButton("New");
		saveButton = new JButton("Save");
		exitButton = new JButton("Exit");
		findButton = new JButton("Find");
		withdrawButton = new JButton("Withdraw");
		depositButton = new JButton("Deposit");
		
		
		/*creating panels and adding items to them*/
		topPanel = new JPanel();
		topPanel.setLayout(new GridLayout(1,4,80,15));
		topPanel.add(nameLabel);
		topPanel.add(nameField);
		topPanel.add(accLabel);
		topPanel.add(accField);
		
		middlePanel = new JPanel();
		middlePanel.setLayout(new GridLayout(1,2,80,15));
		middlePanel.add(addLabel);
		middlePanel.add(addField);
		middlePanel.add(balLabel);
		middlePanel.add(balField);
		
		newPanel = new JPanel();
		newPanel.setLayout(new GridLayout(1,2,80,15));
		newPanel.add(nextButton);
		newPanel.add(lastButton);
		
		bottomPanel = new JPanel();
		bottomPanel.setLayout(new GridLayout(1,3,80,15));
		bottomPanel.add(newButton);
		bottomPanel.add(saveButton);
		bottomPanel.add(exitButton);
		
		labPanel = new JPanel();
		labPanel.setLayout(new GridLayout(1,4,0,80));
		labPanel.add(amtField);
		labPanel.add(findButton);
		labPanel.add(withdrawButton);
		labPanel.add(depositButton);
		
		
		rootPanel = new JPanel();
		rootPanel.setLayout(new GridLayout(5,1,0,80));
		rootPanel.add(topPanel);
		rootPanel.add(middlePanel);
		rootPanel.add(newPanel);
		rootPanel.add(bottomPanel);
		rootPanel.add(labPanel);
		
		Container container = getContentPane();
		
		container.add(rootPanel);
		
		setSize(600,600);
		setVisible(true);
		
		final SavingsAccount S[] = new SavingsAccount[2];
		final CheckingAccount C[] = new CheckingAccount[2];
		
		int j;
		for(j=0;j<2;j++)
		{
			S[j] = new SavingsAccount();
			C[j] = new CheckingAccount(); 
		}
		
		JOptionPane.showMessageDialog(null, "Administrator Mode : Input initial data of 2 Savings accounts");
		for(int i=0;i<2;i++)						/** Creates 5 accounts */
		{
			AccCreator newacc = new AccCreator();
			newacc.set();
			S[i].balance=newacc.balance;
			S[i].accno=newacc.acc+i;
			S[i].pinno=newacc.pin;
			String name = new String();
			String fname = new String();
			String mname = new String();
			String spouse = new String();
			String address = new String();
			String child = new String();
			
			
			/*Getting, checking and saving all the personal information of account holder*/
			name= JOptionPane.showInputDialog("Account Holder's Name?");
			while(!name.matches("[A-Z][a-zA-Z]*"+" "+"[A-Z][a-zA-Z]*"))
			{
				JOptionPane.showMessageDialog(null, "Please enter name in correct fornat i.e Fisrt Last");
				name= JOptionPane.showInputDialog("Account Holder's Name?");
			}
			
			fname= JOptionPane.showInputDialog("Father's Name?");
			while(!fname.matches("[A-Z][a-zA-Z]*"+" "+"[A-Z][a-zA-Z]*"))
			{
				JOptionPane.showMessageDialog(null, "Please enter father's name in correct fornat i.e Fisrt Last");
				fname= JOptionPane.showInputDialog("Father's Name?");
			}
			
			mname = JOptionPane.showInputDialog("Mother's Name?");
			while(!mname.matches("[A-Z][a-zA-Z]*"+" "+"[A-Z][a-zA-Z]*"))
			{
				JOptionPane.showMessageDialog(null, "Please enter mother's name in correct fornat i.e Fisrt Last");
				mname = JOptionPane.showInputDialog("Mother's Name?");
			}
			
			spouse = JOptionPane.showInputDialog("Spouse's Name?(Type Single if not married");
			while(!spouse.matches("[A-Z][a-zA-Z]*"+" "+"[A-Z][a-zA-Z]*") && !spouse.matches("Single"))
			{
				JOptionPane.showMessageDialog(null, "Please enter spouse's name in correct fornat i.e Fisrt Last or Single");
				spouse = JOptionPane.showInputDialog("Spouse's Name?(Type Single if not married");
			}
			
			child = JOptionPane.showInputDialog("Child's Name(Enter 0 if none)?");
			while(!child.matches("[A-Z][a-zA-Z]*"+" "+"[A-Z][a-zA-Z]*") && !child.matches("0"))
			{
				JOptionPane.showMessageDialog(null, "Please enter child's name in correct fornat i.e Fisrt Last or 0");
				child = JOptionPane.showInputDialog("Child's Name(Enter 0 if none)?");
			}
			
			address = JOptionPane.showInputDialog("Address?");
			PersonalInformation newpi = new PersonalInformation(name,address,fname,mname,child,spouse);
			S[i].pinfo=newpi;
		}
		
		/*Entering account data for 5 Checking accounts in Administrator mode*/
		JOptionPane.showMessageDialog(null, "Administrator Mode : Input initial data of 2 Checking accounts");
		for(int i=0;i<2;i++)						/** Creates 5 accounts */
		{
			AccCreator newacc = new AccCreator();
			newacc.set();
			C[i].balance=newacc.balance;
			C[i].accno=newacc.acc+i+2;
			C[i].pinno=newacc.pin;
			String name = new String();
			String fname = new String();
			String mname = new String();
			String spouse = new String();
			String address = new String();
			String child = new String();
			
			/*Getting, checking and saving all the personal information of account holder*/
			name= JOptionPane.showInputDialog("Account Holder's Name?");
			while(!name.matches("[A-Z][a-zA-Z]*"+" "+"[A-Z][a-zA-Z]*"))
			{
				JOptionPane.showMessageDialog(null, "Please enter name in correct fornat i.e Fisrt Last");
				name= JOptionPane.showInputDialog("Account Holder's Name?");
			}
			
			fname= JOptionPane.showInputDialog("Father's Name?");
			while(!fname.matches("[A-Z][a-zA-Z]*"+" "+"[A-Z][a-zA-Z]*"))
			{
				JOptionPane.showMessageDialog(null, "Please enter father's name in correct fornat i.e Fisrt Last");
				fname= JOptionPane.showInputDialog("Father's Name?");
			}
			
			mname = JOptionPane.showInputDialog("Mother's Name?");
			while(!mname.matches("[A-Z][a-zA-Z]*"+" "+"[A-Z][a-zA-Z]*"))
			{
				JOptionPane.showMessageDialog(null, "Please enter mother's name in correct fornat i.e Fisrt Last");
				mname = JOptionPane.showInputDialog("Mother's Name?");
			}
			
			spouse = JOptionPane.showInputDialog("Spouse's Name?(Type Single if not married");
			while(!spouse.matches("[A-Z][a-zA-Z]*"+" "+"[A-Z][a-zA-Z]*") && !spouse.matches("Single"))
			{
				JOptionPane.showMessageDialog(null, "Please enter spouse's name in correct fornat i.e Fisrt Last or Single");
				spouse = JOptionPane.showInputDialog("Spouse's Name?(Type Single if not married");
			}
			
			child = JOptionPane.showInputDialog("Child's Name(Enter 0 if none)?");
			while(!child.matches("[A-Z][a-zA-Z]*"+" "+"[A-Z][a-zA-Z]*") && !child.matches("0"))
			{
				JOptionPane.showMessageDialog(null, "Please enter child's name in correct fornat i.e Fisrt Last or 0");
				child = JOptionPane.showInputDialog("Child's Name(Enter 0 if none)?");
			}
			
			address = JOptionPane.showInputDialog("Address?");
			PersonalInformation newpi = new PersonalInformation(name,address,fname,mname,child,spouse);
			C[i].pinfo=newpi;
		}
		
		
		//Printing to Console the Savings and Checking Accounts created
		System.out.println(S[0].toString());
		System.out.println(S[1].toString());
		System.out.println(C[0].toString());
		System.out.println(C[1].toString());
		
		//Creating the TreeMap and adding the accounts to it
		final TreeMap<Integer, BankAccount> labmap = new TreeMap<Integer, BankAccount>();
		
		labmap.put(S[0].accno, S[0]);
		labmap.put(S[1].accno, S[1]);
		labmap.put(C[0].accno, C[0]);
		labmap.put(C[1].accno, C[1]);
		
		System.out.println(labmap);
		
		depositButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				String accno = accField.getText();
				int accnumber = Integer.parseInt(accno);
				int flag1=0,i;
				for(i=0;i<4;i++)
				{
					if(accnumber==(1200+i))
					{	
						flag1=1;
						break;
					}
				}
				
				if(flag1==0) JOptionPane.showMessageDialog(null, "Account Number not found");
				else
				{
					String amt1;
					double amt=0;
					amt1 = amtField.getText();
					amt = Double.parseDouble(amt1);
					labmap.get(accnumber).creditBalance(amt);
				}
				
			}
		}
		);

		withdrawButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				String accno = accField.getText();
				int accnumber = Integer.parseInt(accno);
				int flag1=0,i;
				for(i=0;i<4;i++)
				{
					if(accnumber==(1200+i))
					{	
						flag1=1;
						break;
					}
				}
				
				if(flag1==0) JOptionPane.showMessageDialog(null, "Account Number not found");
				else
				{
					String amt1;
					double amt=0;
					amt1 = amtField.getText();
					amt = Double.parseDouble(amt1);
					labmap.get(accnumber).debitBalance(amt);
				}
				
			}
		}
		);

		
		findButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				String accno = accField.getText();
				int accnumber = Integer.parseInt(accno);
				int flag1=0,i;
				for(i=0;i<4;i++)
				{
					if(accnumber==(1200+i))
					{	
						flag1=1;
						break;
					}
				}
				
				if(flag1==0) JOptionPane.showMessageDialog(null, "Account Number not found");
				else
				{	
					String s1 = labmap.get(accnumber).toString() + labmap.get(accnumber).pinfo.toString();
					JOptionPane.showMessageDialog(null, s1);
					String name = labmap.get(accnumber).getName();
					String add = labmap.get(accnumber).getAddress();
					double bal = labmap.get(accnumber).getBalance();
					String bala = new Double(bal).toString();
					nameField.setText(name);
					balField.setText(bala);
					addField.setText(add);

				}
				
			}
		}
		);

		
		/*action for on click New button*/
		newButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				/*Nullifying all the text fields*/
				nameField.setText("");
				accField.setText("");
				addField.setText("");
				balField.setText("");
			}
		}
		);
		
		
		
		/*Since given only four Bank Accounts SAVE Button is diabaled and Saving is done in starting*/
		/*action for on click Save button*/
//		saveButton.addActionListener(new ActionListener()
//		{
//			public void actionPerformed(ActionEvent e)
//			{
//				/*Getting information from text fields*/
//				String name = nameField.getText();
//				String accno = accField.getText();
//				String address = addField.getText();
//				String balance = balField.getText();
//				
//				/*checking for the given conditions*/
//				if(name.matches("[A-Z][a-zA-Z]*"+" "+"[A-Z][a-zA-Z]*") && accno.matches("[0-9]{4}") && address.matches("[0-9]{3}"+" "+"[A-Z]{2}"+"-"+"[0-9]") && balance.matches("\\d+"))
//				{
//					try {	
//						
//						statement.executeUpdate("INSERT INTO Table1 VALUES('" + accno + "' , '" + name + "' , '" + balance + "' , '" + address +"')");
//						JOptionPane.showMessageDialog(null, "Entry successfully saved");
//						flag=1;
//						
//					} catch (Exception e1) {
//					// TODO Auto-generated catch block
//					e1.printStackTrace();
//				}
//				}
//				else
//					{
//						/*Printing error*/
//						if(!name.matches("[A-Z][a-zA-Z]*"+" "+"[A-Z][a-zA-Z]*")) System.out.println("Incorrect Name format");
//						if(!accno.matches("[0-9]{4}")) System.out.println("Incorrect Account Number format");
//						if(!address.matches("[0-9]{3}"+" "+"[A-Z]{2}"+"-"+"[0-9]")) System.out.println("Incorrect Address format");
//						if(!balance.matches("\\d+")) System.out.println("Incorrect Balance format");
//						JOptionPane.showMessageDialog(null, "Error");
//					}
//			}
//		}
//		);
		
		/*action for on click Exit button*/
		exitButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				System.exit(0);
			}
		}
		);
		
		/*action for on click >> button*/
		nextButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				try{
					
					int k = 1200 + (counter%4);
					int acno = labmap.get(k).getAccountNo();
					String accno = new Integer(acno).toString();
					String name = labmap.get(k).getName();
					String add = labmap.get(k).getAddress();
					double bal = labmap.get(k).getBalance();
					String bala = new Double(bal).toString();
					accField.setText(accno);
					nameField.setText(name);
					balField.setText(bala);
					addField.setText(add);					
					counter++;

				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		}
		);
		
		/*action for on click >>> button*/
		lastButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				try {
					
					int k = 1203;
					int acno = labmap.get(k).getAccountNo();
					String accno = new Integer(acno).toString();
					String name = labmap.get(k).getName();
					String add = labmap.get(k).getAddress();
					double bal = labmap.get(k).getBalance();
					String bala = new Double(bal).toString();
					accField.setText(accno);
					nameField.setText(name);
					balField.setText(bala);
					addField.setText(add);
				
				} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			}
		}
		);
		
	}
	
	public static void main(String[] args) throws ClassNotFoundException, SQLException {
		// TODO Auto-generated method stub

		Database application = new Database();
		application.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
	}

}
