package BankProgram;

import java.util.TreeMap;

import javax.swing.JOptionPane;

public class CheckingAccount extends BankAccount {

	
	@Override
	public boolean debitBalance(double amount) {
		// TODO Auto-generated method stub
		if(this.getBalance()>=(amount+10))
		{
			this.balance=this.balance-amount-10;
			return true;
		}
		else return false;
	}
	
	@Override
	public double creditBalance(double amount) {
		// TODO Auto-generated method stub
		this.balance=this.balance+amount-10;
		return balance;
	}
	
	public String toString()
	{		
		String s=this.pinfo.toString();
		String s1="\nAccount no = " + this.getAccountNo() + " \nBalance = " + this.getBalance(); 
		System.out.println(s+s1+"\n");
		return s1;
	}

	public static CheckingAccount [] C = new CheckingAccount[2];
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int i;
		for(i=0;i<2;i++)
			C[i] = new CheckingAccount();  
		
		
		/*Entering account data for 2 accounts in Administrator mode*/
		JOptionPane.showMessageDialog(null, "Administrator Mode : Input initial data of 2 accounts");
		for(i=0;i<2;i++)						/** Creates 5 accounts */
		{
			AccCreator newacc = new AccCreator();
			newacc.set();
			C[i].balance=newacc.balance;
			C[i].accno=newacc.acc+i;
			C[i].pinno=newacc.pin;
			String name = new String();
			String fname = new String();
			String mname = new String();
			String spouse = new String();
			String address = new String();
			String child = new String();
			
			/*Getting, checking and saving all the personal information of account holder*/
			name= JOptionPane.showInputDialog("Account Holder's Name?");
			while(!name.matches("[A-Z][a-zA-Z]*"+" "+"[A-Z][a-zA-Z]*"))
			{
				JOptionPane.showMessageDialog(null, "Please enter name in correct fornat i.e Fisrt Last");
				name= JOptionPane.showInputDialog("Account Holder's Name?");
			}
			
			fname= JOptionPane.showInputDialog("Father's Name?");
			while(!fname.matches("[A-Z][a-zA-Z]*"+" "+"[A-Z][a-zA-Z]*"))
			{
				JOptionPane.showMessageDialog(null, "Please enter father's name in correct fornat i.e Fisrt Last");
				fname= JOptionPane.showInputDialog("Father's Name?");
			}
			
			mname = JOptionPane.showInputDialog("Mother's Name?");
			while(!mname.matches("[A-Z][a-zA-Z]*"+" "+"[A-Z][a-zA-Z]*"))
			{
				JOptionPane.showMessageDialog(null, "Please enter mother's name in correct fornat i.e Fisrt Last");
				mname = JOptionPane.showInputDialog("Mother's Name?");
			}
			
			spouse = JOptionPane.showInputDialog("Spouse's Name?(Type Single if not married");
			while(!spouse.matches("[A-Z][a-zA-Z]*"+" "+"[A-Z][a-zA-Z]*") && !spouse.matches("Single"))
			{
				JOptionPane.showMessageDialog(null, "Please enter spouse's name in correct fornat i.e Fisrt Last or Single");
				spouse = JOptionPane.showInputDialog("Spouse's Name?(Type Single if not married");
			}
			
			child = JOptionPane.showInputDialog("Child's Name(Enter 0 if none)?");
			while(!child.matches("[A-Z][a-zA-Z]*"+" "+"[A-Z][a-zA-Z]*") && !child.matches("0"))
			{
				JOptionPane.showMessageDialog(null, "Please enter child's name in correct fornat i.e Fisrt Last or 0");
				child = JOptionPane.showInputDialog("Child's Name(Enter 0 if none)?");
			}
			
			address = JOptionPane.showInputDialog("Address?");
			PersonalInformation newpi = new PersonalInformation(name,address,fname,mname,child,spouse);
			C[i].pinfo=newpi;
		}
		
	
	/*ATM Mode*/
	
	
		String new1;
		int new2;
		
	do 
	{ new1 = JOptionPane.showInputDialog("Welcome to the Bank ATM!!! \nPress 1 to Deposit Money\n Press 2 to Withdraw Money\n Press 3 to Transfer Money\nPress 4 to Exit" );
	new2 = Integer.parseInt(new1);
		
		/*Deposit Option*/
		if(new2==1)
		{
			int j, amt;
			String numb, money;
			while(true){
			numb = JOptionPane.showInputDialog("Please enter account number :");
			while(!numb.matches("[0-9]{4}"))
			{
				JOptionPane.showMessageDialog(null, "Enter a valid Account Number");
				numb = JOptionPane.showInputDialog("Please enter account number :");
			}
			j = Integer.parseInt(numb);
			if(j<1200 | j>1300)JOptionPane.showMessageDialog(null, "Wrong Account Number");
			else if(C[j-1200]==null) JOptionPane.showMessageDialog(null, "Account Number Doesn't exist");
			else break;
			}
				money = JOptionPane.showInputDialog("Please enter the amount to deposit :");
				while(!money.matches("\\d+"))
				{
					JOptionPane.showMessageDialog(null, "Please enter a valid amount");
					money = JOptionPane.showInputDialog("Please enter the amount to deposit :");
				}
				amt = Integer.parseInt(money);
				C[j-1200].balance = C[j-1200].creditBalance(amt);
				JOptionPane.showMessageDialog(null, "Final Balance of account of "+C[j-1200].pinfo.getName()+ " is " + C[j-1200].getBalance());
		}
		/*Withdraw Option*/
		else if(new2==2)
		{
			int j, k, sum;
			String numb, money, pi;
			while(true){
				numb = JOptionPane.showInputDialog("Please enter account number :");
				while(!numb.matches("[0-9]{4}"))
				{
					JOptionPane.showMessageDialog(null, "Enter a valid Account Number");
					numb = JOptionPane.showInputDialog("Please enter account number :");
				}
				j = Integer.parseInt(numb);
				if(j<1200 | j>1300)JOptionPane.showMessageDialog(null, "Wrong Account Number");
				else if(C[j-1200]==null) JOptionPane.showMessageDialog(null, "Account Number Doesn't exist");
				else break;
				}
			pi = JOptionPane.showInputDialog(C[j-1200].pinfo.getName()+" please enter your PIN number");
			while(!pi.matches("[0-9]{4}"))
			{
				JOptionPane.showMessageDialog(null, "Enter a 4 digit pin");
				pi = JOptionPane.showInputDialog(C[j-1200].pinfo.getName()+" please enter your PIN number");
			}
			k = Integer.parseInt(pi);
			if(k!=C[j-1200].pinno) JOptionPane.showMessageDialog(null, "Wrong PIN");
			else 
			{
				money = JOptionPane.showInputDialog("Please enter amount :");
				while(!money.matches("\\d+"))
				{
					JOptionPane.showMessageDialog(null, "Please enter a valid amount");
					money = JOptionPane.showInputDialog("Please enter the amount to deposit :");
				}
				sum = Integer.parseInt(money);
				if(sum<=C[j-1200].balance)
					{
						C[j-1200].debitBalance(sum);
						JOptionPane.showMessageDialog(null, "Withdrawal Successful\n"+"Final Balance of account of "+C[j-1200].pinfo.getName()+ " is " + C[j-1200].getBalance());
					}
				else JOptionPane.showMessageDialog(null, "Insufficient balance");
			}
		}
		
		/*Transfer Option*/
		else if(new2==3)
		{
			int j, k, sum;
			String numb, money, pi;
			while(true){
				numb = JOptionPane.showInputDialog("Please enter account number :");
				while(!numb.matches("[0-9]{4}"))
				{
					JOptionPane.showMessageDialog(null, "Enter a valid Account Number");
					numb = JOptionPane.showInputDialog("Please enter account number :");
				}
				j = Integer.parseInt(numb);
				if(j<1200 | j>1300)JOptionPane.showMessageDialog(null, "Wrong Account Number");
				else if(C[j-1200]==null) JOptionPane.showMessageDialog(null, "Account Number Doesn't exist");
				else break;
				}
			pi = JOptionPane.showInputDialog(C[j-1200].pinfo.getName()+" please enter your PIN number");
			while(!pi.matches("[0-9]{4}"))
			{
				JOptionPane.showMessageDialog(null, "Enter a 4 digit pin");
				pi = JOptionPane.showInputDialog(C[j-1200].pinfo.getName()+" please enter your PIN number");
			}
			k = Integer.parseInt(pi);
			if(k!=C[j-1200].pinno) JOptionPane.showMessageDialog(null, "Wrong PIN");
			else 
			{
				String next;
				int newj;
				while(true){
					next = JOptionPane.showInputDialog("Please enter account number to transfer :");
					while(!next.matches("[0-9]{4}"))
					{
						JOptionPane.showMessageDialog(null, "Enter a valid Account Number");
						next = JOptionPane.showInputDialog("Please enter account number to transfer :");
					}
					newj = Integer.parseInt(next);
					if(newj<1200 | newj>1300)JOptionPane.showMessageDialog(null, "Wrong Account Number");
					else if(C[newj-1200]==null) JOptionPane.showMessageDialog(null, "Account Number Doesn't exist");
					else break;
					}
				money = JOptionPane.showInputDialog("Please enter amount :");
				while(!money.matches("\\d+"))
				{
					JOptionPane.showMessageDialog(null, "Please enter a valid amount");
					money = JOptionPane.showInputDialog("Please enter the amount to deposit :");
				}
				sum = Integer.parseInt(money);
				if(sum<=C[j-1200].balance) 
					{
						C[j-1200].debitBalance(sum);
						C[newj-1200].creditBalance(sum);
						JOptionPane.showMessageDialog(null, "Transfer Successful\n"+C[j-1200].pinfo.getName()+"'s new balance = "+C[j-1200].getBalance()+"\n"+C[newj-1200].pinfo.getName()+"'s new balance = "+C[newj-1200].getBalance());
					}
				else JOptionPane.showMessageDialog(null, "Insufficient balance");
			}
		}
		else if(new2!=4)
		{
			JOptionPane.showMessageDialog(null, "Please enter a valid choice");
		}
	} while (new2!=4);

	/*TreeMap<Integer, CheckingAccount> newmap = new TreeMap<Integer, CheckingAccount>();
	newmap.put(C[0].accno,C[0]);
	newmap.put(C[1].accno, C[1]);
	
	System.out.println(newmap);*/
	
	}
}
