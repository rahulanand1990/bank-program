package BankProgram;

public abstract class BankAccount implements BankInterface, Cloneable {

	protected PersonalInformation pinfo;
	protected int pinno;
	protected int accno;
	protected double balance;
		
	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return this.pinfo.getName();
	}

	@Override
	public void setName(String name) {
		// TODO Auto-generated method stub
		this.pinfo.setName(name);
	}

	@Override
	public int getAccountNo() {
		// TODO Auto-generated method stub
		return this.accno;
	}

	@Override
	public void setAccountNo(int ac) {
		// TODO Auto-generated method stub
		this.accno=ac;
	}

	@Override
	public String getAddress() {
		// TODO Auto-generated method stub
		return this.pinfo.getAddress();
	}

	@Override
	public void setAddress(String addr) {
		// TODO Auto-generated method stub
		this.pinfo.setAddress(addr);
	}

	@Override
	public double getBalance() {
		// TODO Auto-generated method stub
		return this.balance;
	}

	@Override
	public void setBalance(double b) {
		// TODO Auto-generated method stub
		this.balance=b;
	}

	@Override
	public double creditBalance(double amount) {
		// TODO Auto-generated method stub
		this.balance=this.balance+amount;
		return balance;
	}
}
