package BankProgram;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

import javax.swing.JOptionPane;

public class SavingsAccount extends BankAccount {
	
	float rateOfInterest;
	float interest;
	
	
	/*Add Interest Method*/
	public void addInterest() {
		// TODO Auto-generated method stub
		this.balance=this.balance+this.interest;;
	}

	/*To check whether the account holder has enough balance to withdraw or transfer*/
	@Override
	public boolean debitBalance(double amount) {
		// TODO Auto-generated method stub
		if(this.getBalance()>=amount)
		{
			this.balance=this.balance-amount;
			return true;
		}
		else return false;
	}

	/*toString method to output the account details*/
	public String toString()
	{		
		String s=this.pinfo.toString();
		String s1="\nAccount no = " + this.getAccountNo() + " \nBalance = " + this.getBalance(); 
		System.out.println(s+s1+"\n");
		return s1;
	}
	
	/*Equals method to check whether to accounts are same or not*/
	public boolean equals1(Object obj){
		SavingsAccount bk = (SavingsAccount) obj;
		if(this.pinfo.getName()==bk.pinfo.getName() && this.pinfo.getAddress()==bk.pinfo.getAddress())
			return true;
		return false;
	}
	
	/*Compare method to compare bank accounts on the basis of balance*/
	public static void compare(){
		for(int i=0;i<5;i++)
		{
			for(int j=0;j<5;j++)
			{
				if(S[i%5].balance>S[j%5].balance)
					System.out.println(S[i].pinfo.getName() + " has more balance than "+S[j].pinfo.getName());
			}
		}
	}
	
	/**
	 * @param args
	 */
	
	/*Declaring an array of savings accounts*/
	public static SavingsAccount[] S = new SavingsAccount[100];
	
	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		
		/*Initialising 5 accounts*/
		int i;
		for(i=0;i<5;i++)
			S[i] = new SavingsAccount();  
		
		/*Creating output file to print details*/
		Writer output = null;
		File file = new File("accountDetails.txt");
		
		/*Entering account data for 5 accounts in Administrator mode*/
		JOptionPane.showMessageDialog(null, "Administrator Mode : Input initial data of 5 accounts");
		for(i=0;i<5;i++)						/** Creates 5 accounts */
		{
			AccCreator newacc = new AccCreator();
			newacc.set();
			S[i].balance=newacc.balance;
			S[i].accno=newacc.acc+i;
			S[i].pinno=newacc.pin;
			String name = new String();
			String fname = new String();
			String mname = new String();
			String spouse = new String();
			String address = new String();
			String child = new String();
			
			/*Getting, checking and saving all the personal information of account holder*/
			name= JOptionPane.showInputDialog("Account Holder's Name?");
			while(!name.matches("[A-Z][a-zA-Z]*"+" "+"[A-Z][a-zA-Z]*"))
			{
				JOptionPane.showMessageDialog(null, "Please enter name in correct fornat i.e Fisrt Last");
				name= JOptionPane.showInputDialog("Account Holder's Name?");
			}
			
			fname= JOptionPane.showInputDialog("Father's Name?");
			while(!fname.matches("[A-Z][a-zA-Z]*"+" "+"[A-Z][a-zA-Z]*"))
			{
				JOptionPane.showMessageDialog(null, "Please enter father's name in correct fornat i.e Fisrt Last");
				fname= JOptionPane.showInputDialog("Father's Name?");
			}
			
			mname = JOptionPane.showInputDialog("Mother's Name?");
			while(!mname.matches("[A-Z][a-zA-Z]*"+" "+"[A-Z][a-zA-Z]*"))
			{
				JOptionPane.showMessageDialog(null, "Please enter mother's name in correct fornat i.e Fisrt Last");
				mname = JOptionPane.showInputDialog("Mother's Name?");
			}
			
			spouse = JOptionPane.showInputDialog("Spouse's Name?(Type Single if not married");
			while(!spouse.matches("[A-Z][a-zA-Z]*"+" "+"[A-Z][a-zA-Z]*") && !spouse.matches("Single"))
			{
				JOptionPane.showMessageDialog(null, "Please enter spouse's name in correct fornat i.e Fisrt Last or Single");
				spouse = JOptionPane.showInputDialog("Spouse's Name?(Type Single if not married");
			}
			
			child = JOptionPane.showInputDialog("Child's Name(Enter 0 if none)?");
			while(!child.matches("[A-Z][a-zA-Z]*"+" "+"[A-Z][a-zA-Z]*") && !child.matches("0"))
			{
				JOptionPane.showMessageDialog(null, "Please enter child's name in correct fornat i.e Fisrt Last or 0");
				child = JOptionPane.showInputDialog("Child's Name(Enter 0 if none)?");
			}
			
			address = JOptionPane.showInputDialog("Address?");
			PersonalInformation newpi = new PersonalInformation(name,address,fname,mname,child,spouse);
			S[i].pinfo=newpi;
		}
		
		/*Printing all account details using toString Method*/
		for(i=0;i<5;i++)
		{
			S[i].toString();
		}
		
		
		/*ATM Mode*/
		String new1;
		int new2;
		do 
		{ new1 = JOptionPane.showInputDialog("Welcome to the Bank ATM!!! \nPress 1 to Deposit Money\n Press 2 to Withdraw Money\n Press 3 to Transfer Money\nPress 4 to Exit" );
		new2 = Integer.parseInt(new1);
			
			/*Deposit Option*/
			if(new2==1)
			{
				int j, amt;
				String numb, money;
				while(true){
				numb = JOptionPane.showInputDialog("Please enter account number :");
				while(!numb.matches("[0-9]{4}"))
				{
					JOptionPane.showMessageDialog(null, "Enter a valid Account Number");
					numb = JOptionPane.showInputDialog("Please enter account number :");
				}
				j = Integer.parseInt(numb);
				if(j<1200 | j>1300)JOptionPane.showMessageDialog(null, "Wrong Account Number");
				else if(S[j-1200]==null) JOptionPane.showMessageDialog(null, "Account Number Doesn't exist");
				else break;
				}
					money = JOptionPane.showInputDialog("Please enter the amount to deposit :");
					while(!money.matches("\\d+"))
					{
						JOptionPane.showMessageDialog(null, "Please enter a valid amount");
						money = JOptionPane.showInputDialog("Please enter the amount to deposit :");
					}
					amt = Integer.parseInt(money);
					S[j-1200].balance = S[j-1200].creditBalance(amt);
					JOptionPane.showMessageDialog(null, "Final Balance of account of "+S[j-1200].pinfo.getName()+ " is " + S[j-1200].getBalance());
			}
			/*Withdraw Option*/
			else if(new2==2)
			{
				int j, k, sum;
				String numb, money, pi;
				while(true){
					numb = JOptionPane.showInputDialog("Please enter account number :");
					while(!numb.matches("[0-9]{4}"))
					{
						JOptionPane.showMessageDialog(null, "Enter a valid Account Number");
						numb = JOptionPane.showInputDialog("Please enter account number :");
					}
					j = Integer.parseInt(numb);
					if(j<1200 | j>1300)JOptionPane.showMessageDialog(null, "Wrong Account Number");
					else if(S[j-1200]==null) JOptionPane.showMessageDialog(null, "Account Number Doesn't exist");
					else break;
					}
				pi = JOptionPane.showInputDialog(S[j-1200].pinfo.getName()+" please enter your PIN number");
				while(!pi.matches("[0-9]{4}"))
				{
					JOptionPane.showMessageDialog(null, "Enter a 4 digit pin");
					pi = JOptionPane.showInputDialog(S[j-1200].pinfo.getName()+" please enter your PIN number");
				}
				k = Integer.parseInt(pi);
				if(k!=S[j-1200].pinno) JOptionPane.showMessageDialog(null, "Wrong PIN");
				else 
				{
					money = JOptionPane.showInputDialog("Please enter amount :");
					while(!money.matches("\\d+"))
					{
						JOptionPane.showMessageDialog(null, "Please enter a valid amount");
						money = JOptionPane.showInputDialog("Please enter the amount to deposit :");
					}
					sum = Integer.parseInt(money);
					if(sum<=S[j-1200].balance)
						{
							S[j-1200].debitBalance(sum);
							JOptionPane.showMessageDialog(null, "Withdrawal Successful\n"+"Final Balance of account of "+S[j-1200].pinfo.getName()+ " is " + S[j-1200].getBalance());
						}
					else JOptionPane.showMessageDialog(null, "Insufficient balance");
				}
			}
			
			/*Transfer Option*/
			else if(new2==3)
			{
				int j, k, sum;
				String numb, money, pi;
				while(true){
					numb = JOptionPane.showInputDialog("Please enter account number :");
					while(!numb.matches("[0-9]{4}"))
					{
						JOptionPane.showMessageDialog(null, "Enter a valid Account Number");
						numb = JOptionPane.showInputDialog("Please enter account number :");
					}
					j = Integer.parseInt(numb);
					if(j<1200 | j>1300)JOptionPane.showMessageDialog(null, "Wrong Account Number");
					else if(S[j-1200]==null) JOptionPane.showMessageDialog(null, "Account Number Doesn't exist");
					else break;
					}
				pi = JOptionPane.showInputDialog(S[j-1200].pinfo.getName()+" please enter your PIN number");
				while(!pi.matches("[0-9]{4}"))
				{
					JOptionPane.showMessageDialog(null, "Enter a 4 digit pin");
					pi = JOptionPane.showInputDialog(S[j-1200].pinfo.getName()+" please enter your PIN number");
				}
				k = Integer.parseInt(pi);
				if(k!=S[j-1200].pinno) JOptionPane.showMessageDialog(null, "Wrong PIN");
				else 
				{
					String next;
					int newj;
					while(true){
						next = JOptionPane.showInputDialog("Please enter account number to transfer :");
						while(!next.matches("[0-9]{4}"))
						{
							JOptionPane.showMessageDialog(null, "Enter a valid Account Number");
							next = JOptionPane.showInputDialog("Please enter account number to transfer :");
						}
						newj = Integer.parseInt(next);
						if(newj<1200 | newj>1300)JOptionPane.showMessageDialog(null, "Wrong Account Number");
						else if(S[newj-1200]==null) JOptionPane.showMessageDialog(null, "Account Number Doesn't exist");
						else break;
						}
					money = JOptionPane.showInputDialog("Please enter amount :");
					while(!money.matches("\\d+"))
					{
						JOptionPane.showMessageDialog(null, "Please enter a valid amount");
						money = JOptionPane.showInputDialog("Please enter the amount to deposit :");
					}
					sum = Integer.parseInt(money);
					if(sum<=S[j-1200].balance) 
						{
							S[j-1200].debitBalance(sum);
							S[newj-1200].creditBalance(sum);
							JOptionPane.showMessageDialog(null, "Transfer Successful\n"+S[j-1200].pinfo.getName()+"'s new balance = "+S[j-1200].getBalance()+"\n"+S[newj-1200].pinfo.getName()+"'s new balance = "+S[newj-1200].getBalance());
						}
					else JOptionPane.showMessageDialog(null, "Insufficient balance");
				}
			}
			else if(new2!=4)
			{
				JOptionPane.showMessageDialog(null, "Please enter a valid choice");
			}
		} while (new2!=4);
		
		/*Check cumulative balance > 10 lacs*/
		double cum[] =  new double[5];
		int l=0,k=0;
		for(l=0;l<5;l++)
		{
			for(k=0;k<5;k++)
			{
				if(S[l].pinfo==S[k].pinfo && l!=k) cum[l]=cum[l]+S[l].getBalance()+S[k].getBalance();
			}
		}
		
		for(l=0;l<5;l++)
		{
			if(cum[l]>1000000) System.out.println(S[1200+l].getName()+" having account number "+S[1200+l].getAccountNo()+" has balance "+cum[l]);
		}
	
		/*Printing all accounts before exiting*/
		output = new BufferedWriter(new FileWriter(file));
		for(i=0;i<5;i++) {
			String text = S[i].toString();
			String text1 = S[i].pinfo.toString();
			output.write(text);
			((BufferedWriter) output).newLine();
			output.write(text1);
			((BufferedWriter) output).newLine();
			((BufferedWriter) output).newLine();
		}
			output.close();
			
		/*Calling compare method to compare accounts on basis of balance*/
		compare();
		

	}
}
 