package BankProgram;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class ReadAccountDetails {

	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		BufferedReader in = new BufferedReader(new FileReader("accountDetails.txt"));
	    String str;
	    while ((str = in.readLine()) != null) {
	        System.out.println(str);
	    }
	    in.close();
}
}
