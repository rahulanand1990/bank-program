package BankProgram;

import javax.swing.JOptionPane;

public class AccCreator {

	int acc;
	double balance;
	int pin;
	private final static int counter = 1200;
	static String s1, s2;
	
		
		
	void set()						/** Account details function */
	{
		s1 = JOptionPane.showInputDialog("Initial balance?");
		while(!s1.matches("\\d+"))
		{
			JOptionPane.showMessageDialog(null, "Enter correct balance");
			s1 = JOptionPane.showInputDialog("Initial balance?");
		}
		balance = Integer.parseInt(s1); 
			
		s2 = JOptionPane.showInputDialog("Please enter a 4 digit pin number");
		while(!s2.matches("[0-9]{4}"))
		{
			JOptionPane.showMessageDialog(null, "Enter correct pin format");
			s2 = JOptionPane.showInputDialog("Please enter a 4 digit pin number");
		}
				
		pin = Integer.parseInt(s2);
		acc=counter;
	}
}
